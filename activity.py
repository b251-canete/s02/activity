year = int(input("Please input a year\n"))

if year <= 0:
	print(f"Invalid input, year cannot be 0 and below!")
elif year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")


rows = int(input(f"Enter number of rows\n"))
columns = int(input(f"Enter number of columns\n"))


# [SOLUTION NO. 1]

# i = 1

# while i <= rows:
# 	print(f'*'*columns)
# 	i += 1


# [SOLUTION NO. 2]

for x in range(rows):
	for y in range(columns):
		if y == columns - 1:
			print(f"*")
		else:
			print(f"*", end='')